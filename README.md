# Kafka Dummie

Projeto dummie completamente errado, 'quickstart' para visualização de comunicação entre spring boot e apache kafka...

Este projeto utiliza:
* Java 8
* Docker
  * Kafka 2.5.0
  * Zookeeper

## Kafka Server
Compose docker configurado com kafka e zookeeper na pasta ** docker ** localizada na raíz do repositório.

```` shell
    $ docker-compose -f docker-compose-dev.yml up -d
```` 

#### Conectar-se ao Kafka console consumer

```` shell
    $ docker exec -t kafka_container kafka-console-consumer.sh --bootstrap-server :9092 --topic topic_name
    $ docker exec -t kafka kafka-console-consumer.sh --bootstrap-server :9092 --topic topicoQualquer
```` 

---

## Referências
- [wurstmeister/kafka-docker](https://github.com/wurstmeister/kafka-docker)
- [Running Kafka Broker in Docker](https://jaceklaskowski.gitbooks.io/apache-kafka/kafka-docker.html)  
- [Apache Kafka and Spring Boot (Consumer, Producer)](https://www.udemy.com/course/apache-kafka-and-spring-boot-consumer-producer)