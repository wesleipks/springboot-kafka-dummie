package wes.exemplo.kafkadummie.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import wes.exemplo.kafkadummie.models.MessageModel;
import wes.exemplo.kafkadummie.services.Producer;

@RestController
@RequestMapping("/api/kafka")
public class KafkaDummieController {

    private static final Logger logger = LoggerFactory.getLogger(KafkaDummieController.class);

    private final Producer producer;

    @Autowired
    KafkaDummieController(Producer producer) {
        this.producer = producer;
    }

    @PostMapping
    public void post(@RequestBody MessageModel messageModel) {
        logger.info("[####] Post :: {} ", messageModel);
        this.producer.sendMessage(messageModel);
    }

}
