package wes.exemplo.kafkadummie.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import wes.exemplo.kafkadummie.models.MessageModel;

@Service
public class Consumer {

    private static final Logger logger = LoggerFactory.getLogger(Consumer.class);

    @KafkaListener(topics = "topicoqualquer", groupId = "group_id")
    public void consume(MessageModel messageModel) {
        logger.info("[####] Consumer :: {}", messageModel);
        //-- ...
        //-- Executa qualquer coisa que um consumer deveria executar no listener
    }

}
