package wes.exemplo.kafkadummie.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import wes.exemplo.kafkadummie.models.MessageModel;

@Service
public class Producer {

    private static final Logger logger = LoggerFactory.getLogger(Producer.class);
    public static final String TOPIC = "topicoqualquer";

    @Autowired
    private KafkaTemplate<String, MessageModel> kafkaTemplate;

    public void sendMessage(MessageModel messageModel) {
        logger.info("[####] Producer :: {}", messageModel);
        this.kafkaTemplate.send(TOPIC, messageModel);
    }

}
