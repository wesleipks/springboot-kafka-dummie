package wes.exemplo.kafkadummie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaDummieApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaDummieApplication.class, args);
    }

}
